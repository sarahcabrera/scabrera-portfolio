import React, {useState} from 'react';

import Typography from '@material-ui/core/Typography';
import { makeStyles, withStyles  } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Fab from '@material-ui/core/Fab';

import Zoom from '@material-ui/core/Zoom';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';

import BurstModeIcon from '@material-ui/icons/BurstMode';
import Avatar from '@material-ui/core/Avatar';
import { Link, NavLink } from 'react-router-dom';
import Badge from '@material-ui/core/Badge';


import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { Switch, FormControlLabel } from '@material-ui/core';
import Brightness4TwoToneIcon from '@material-ui/icons/Brightness4TwoTone';
import ButtonGroup from '@material-ui/core/ButtonGroup';


import {v1 as uuid} from 'uuid';

import Toolbar from '@material-ui/core/Toolbar';
import Divider from '@material-ui/core/Divider';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Grow from '@material-ui/core/Grow';


const useStyles = makeStyles((theme) => ({
	  card: {
	  	width: '110%',
	  	marginTop: 10,
	  	animation: '$ripple .4s 1 ease-in-out',
	  },
	  '@keyframes ripple': {
		    '0%': {
		      transform: 'scale(.7)',
		      opacity: 0,
		    },
		    '100%': {
		      transform: 'scale(1)',
		      bottom: 0,
		      opacity: 0.8,
		    },
  	},
  	bitmoji: {
  		opacity: theme.palette.type == 'light' ? 0 : 1,
  		width: 100,
  		height: 100,
  		borderRadius: "50%",
  		border: '1px solid #ff9e80',
  		marginLeft: 450,
  		animation: '$ripple2 1s 1 ease-in-out',
  	},
  	 '@keyframes ripple2': {
		    '0%': {
		      transform: 'scale(.8)',
		      bottom: 0,
		      opacity: 0,
		    },
		    '100%': {
		      transform: 'scale(1.5)',
		      bottom: 50,
		      opacity: 0.8,
		    },
	}
	
}));




const Landing = ({theme}) => {

	const classes = useStyles();

  const [checked, setChecked] = React.useState(false);

  const handleChange = () => {
    setChecked((prev) => !prev);
  };

  
  return (
  		 
  		<React.Fragment >
  			<CssBaseline  />

			
  			  <Card className={classes.card}
  			  			variant="outlined" 
  			  			style={{ borderRadius: 30,
  			  								backgroundColor: "transparent",
	  										padding: 20,
      										border: '1px solid #ff9e80'}}>
			      <CardContent>
			      		

			      		<img src="https://scabrera-portfolio.s3.us-east-1.amazonaws.com/bitmojihello.jpg" 
				 className={classes.bitmoji} align="center"/>
			      		<Toolbar>
					        <Typography 
		  								variant='h2'> Sarah Cabrera</Typography>
					  		
					  		
			      		</Toolbar> 
			      		<Divider variant="middle" />
			      		<br />
			      		<Typography variant="h6" 
			      			style={{
			      				color: theme.palette.type === 'light' ? '#870000' : '#ff9100',
			      				marginLeft: 20}}>
			      				Fullstack Web Developer | Privacy Professional | Lawyer
			      		</Typography>
			      </CardContent>
			      <CardActions>
			      	  
				    
			      </CardActions>
		    	</Card>
  			

			</React.Fragment>

  )
}

export default Landing;