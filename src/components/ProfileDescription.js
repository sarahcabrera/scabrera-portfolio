import React from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { makeStyles, withStyles, useTheme  } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Grow from '@material-ui/core/Grow';
import Toolbar from '@material-ui/core/Toolbar';
import Avatar from '@material-ui/core/Avatar';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Zoom from '@material-ui/core/Zoom';

import MobileStepper from '@material-ui/core/MobileStepper';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

import LinkedInIcon from '@material-ui/icons/LinkedIn';
import InstagramIcon from '@material-ui/icons/Instagram';





const useStyles = makeStyles((theme) => ({

  header: {
    display: 'flex',
    alignItems: 'center',
    height: 35,
    paddingLeft: theme.spacing(4),
    backgroundColor: theme.palette.background.default,
    borderRadius: 15,

  },
  paper: {
  	background: theme.palette.type == 'light' ? 'linear-gradient(30deg, #ffd6c9 50%, ivory 90%)' : 'linear-gradient(30deg, #2c387e 60%, #e57373 90%)',
  	padding: 20,
  	borderRadius: 10,
  	height: 700

  },
  avatar: {
    width: theme.spacing(25),
    height: theme.spacing(25),
    margin: 30,
    border: 'thin solid #f9a825'

  },
  skills: {
	  width: theme.spacing(11),
	  height: theme.spacing(11),
	  margin: 10,
	  boxShadow: ' 1px 1px 1px 1px ' + theme.palette.background.default,
   borderRadius: 10,
  },
	
	
}));

function AppearOnScroll(props) {
	  const { children, window } = props;
	  const trigger = useScrollTrigger({
		    target: window ? window() : undefined,
		    disableHysteresis: true,
		    threshold: 50
	   });

	  return (
	    <Zoom in={trigger}
	    		style={{ transitionDelay: trigger ? '400ms' : '0ms' }}
	    >
	      <div >
	        {children}
	      </div>
	    </Zoom>
	  );
}

function AppearLaterOnScroll1(props) {
	  const { children, window } = props;
	  const trigger = useScrollTrigger({
		    target: window ? window() : undefined,
		    disableHysteresis: true,
		    threshold: 80
	   });

	  return (
	    <Zoom in={trigger}
	    		style={{ transitionDelay: trigger ? '750ms' : '0ms' }}
	    >
	      <div >
	        {children}
	      </div>
	    </Zoom>
	  );
}

function AppearLaterOnScroll2(props) {
	  const { children, window } = props;
	  const trigger = useScrollTrigger({
		    target: window ? window() : undefined,
		    disableHysteresis: true,
		    threshold: 80
	   });

	  return (
	    <Zoom in={trigger}
	    		style={{ transitionDelay: trigger ? '850ms' : '0ms' }}
	    >
	      <div >
	        {children}
	      </div>
	    </Zoom>
	  );
}

function AppearLaterOnScroll3(props) {
	  const { children, window } = props;
	  const trigger = useScrollTrigger({
		    target: window ? window() : undefined,
		    disableHysteresis: true,
		    threshold: 80
	   });

	  return (
	    <Zoom in={trigger}
	    		style={{ transitionDelay: trigger ? '950ms' : '0ms' }}
	    >
	      <div >
	        {children}
	      </div>
	    </Zoom>
	  );
}

function AppearLaterOnScroll4(props) {
	  const { children, window } = props;
	  const trigger = useScrollTrigger({
		    target: window ? window() : undefined,
		    disableHysteresis: true,
		    threshold: 80
	   });

	  return (
	    <Zoom in={trigger}
	    		style={{ transitionDelay: trigger ? '1050ms' : '0ms' }}
	    >
	      <div >
	        {children}
	      </div>
	    </Zoom>
	  );
}

function AppearLaterOnScroll5(props) {
	  const { children, window } = props;
	  const trigger = useScrollTrigger({
		    target: window ? window() : undefined,
		    disableHysteresis: true,
		    threshold: 80
	   });

	  return (
	    <Zoom in={trigger}
	    		style={{ transitionDelay: trigger ? '1150ms' : '0ms' }}
	    >
	      <div >
	        {children}
	      </div>
	    </Zoom>
	  );
}

function AppearLaterOnScroll6(props) {
	  const { children, window } = props;
	  const trigger = useScrollTrigger({
		    target: window ? window() : undefined,
		    disableHysteresis: true,
		    threshold: 80
	   });

	  return (
	    <Zoom in={trigger}
	    		style={{ transitionDelay: trigger ? '1250ms' : '0ms' }}
	    >
	      <div >
	        {children}
	      </div>
	    </Zoom>
	  );
}

const ProfileDescription = ({theme}) => {

  const classes = useStyles();




  return (

    <React.Fragment>
    	<AppearOnScroll >
    	 <Paper elevation={30}
    	 		className={classes.paper}
    	 	>

			      		<Toolbar>
			      					
					      		  <Avatar alt="S.Cabrera" 
							      			src="https://scabrera-portfolio.s3.us-east-1.amazonaws.com/stcprofile9.jpg"  
							      			variant="circle" 
							      			className={classes.avatar}
							      		
						      			/> 
					      			<Box>
							      		<Typography variant="h6" 
							      			align="justify"
							      			style={{marginLeft: 20,
							      				color: theme.palette.type == 'light' ? 'black' : 'white'
							      			}}
							      		>
							      			Sarah is fullstack web developer, privacy professional (CIPM and CIPP/E), lawyer, and psychology major who loves painting, design, cooking, and gardening.
							      		</Typography>

							      		<br/><br/>

							      		 		

								               <Toolbar>
								               
								                <Typography variant="overline" align="center">
								                    Connect via  
								                  </Typography>
								                  
								                  <a href="https://www.linkedin.com/in/sarah-c-975734b/" 
								                  			target="_blank">
								                    <LinkedInIcon size="large" 
								                    	style={{margin:5, 
								                    		width: 50, 
								                    		height: 50,
								                    		color: 'dodgerblue'
								                    	}} />
								                  </a>

								                  <a href="https://www.instagram.com/sarahcabdev/" 
								                  			target="_blank">
								                    <InstagramIcon size="large" 
								                    	style={{margin:5, 
								                    		width: 50, 
								                    		height: 50,
								                    		color: 'magenta'
								                    	}} />
								                  </a>
								               

								               
								               </Toolbar> 

							      		<Divider />


				  						</Box>
			      		</Toolbar> 

			      		
			      		<br /><br />

			      		<Toolbar >
				      			
				      			<Box width="100%" >
						      			<Typography variant="h6" 
						      									align="center"

						      			>
						      					Skills
						      			</Typography>
				      			</Box>
			      		</Toolbar>		

			      		<Toolbar >
			      			
					      		
		  					
		  					
				      			<Box width="100%">
						      		<Typography variant="h6" 
						      								align="center"
						      					
						      		>
						      
						      		</Typography>
						      		
						      		<Toolbar>
							      		<AppearLaterOnScroll1 >
							      				<Avatar 
							      				variant="rounded"
							      				className={classes.skills}
							      				src="https://scabrera-portfolio.s3.amazonaws.com/html5.png"
							      		/>
							      		</AppearLaterOnScroll1>

							      		<AppearLaterOnScroll2 >
								      		<Avatar 
								      				variant="rounded"
								      				className={classes.skills}
								      				src="https://image.flaticon.com/icons/png/512/919/919826.png"
								      		/>
						      			</AppearLaterOnScroll2>

						      			<AppearLaterOnScroll3 >
								      		<Avatar 
								      				variant="rounded"
								      				className={classes.skills}
								      				src="https://www.pngkit.com/png/detail/954-9549328_bootstrap-featured-image-bootstrap-3-logo-png.png"
								      		/>
								      	</AppearLaterOnScroll3>

								      	<AppearLaterOnScroll4 >
								      		<Avatar 
								      				variant="rounded"
								      				className={classes.skills}
								      				src="https://www.pngitem.com/pimgs/m/116-1167737_logo-javascript-pattern-copyright-framework-free-download-javascript.png"
								      		/>
								      	</AppearLaterOnScroll4>

								      	<AppearLaterOnScroll5 >
								      		<Avatar 
								      				variant="rounded"
								      				className={classes.skills}
								      				src="https://openjsf.org/wp-content/uploads/sites/84/2019/10/jquery-logo-vertical_large_square.png"
								      		/>
								      	</AppearLaterOnScroll5>

								      	<AppearLaterOnScroll6 >
						      				<Avatar 
						      				variant="rounded"
						      				className={classes.skills}
						      				src="https://pbs.twimg.com/profile_images/477413657693782016/Ch8Mjsdv_400x400.png"
						      				/>
						      		</AppearLaterOnScroll6>

						      		<AppearLaterOnScroll5 >
								      		<Avatar 
								      				variant="rounded"
								      				className={classes.skills}
								      				src="https://mmb.irbbarcelona.org/formacio/~dbw08/img/logo_mysql.jpeg"
								      		/>
								      </AppearLaterOnScroll5>

								      <AppearLaterOnScroll4 >
							      		<Avatar 
							      				variant="rounded"
							      				className={classes.skills}
							      				src="https://res.cloudinary.com/practicaldev/image/fetch/s--pYTaUTHE--/c_imagga_scale,f_auto,fl_progressive,h_1080,q_auto,w_1080/https://dev-to-uploads.s3.amazonaws.com/i/hdcryrf4hqvt9uhui4l5.png"
							      		/>
							      	</AppearLaterOnScroll4>

						      		</Toolbar>

						      		<Toolbar>

						      		

							      	 <AppearLaterOnScroll3 >
								      		<Avatar 
								      				variant="rounded"
								      				className={classes.skills}
								      				src="https://cdn.app.compendium.com/uploads/user/e7c690e8-6ff9-102a-ac6d-e4aebca50425/f4a5b21d-66fa-4885-92bf-c4e81c06d916/Image/e5eee315a17de0d7f56117077eb71fa9/mongo.png"
								      		/>
								      </AppearLaterOnScroll3>

								       <AppearLaterOnScroll2 >
								      		<Avatar 
								      				variant="rounded"
								      				className={classes.skills}
								      				src="https://www.ateamindia.com/wp-content/uploads/2019/03/main-qimg-f406db5658b5d0dade4d70a989560439.png"
								      		/>
								       </AppearLaterOnScroll2>

								       <AppearLaterOnScroll6 >
							      				<Avatar 
							      				variant="rounded"
							      				className={classes.skills}
							      				src="https://image.flaticon.com/icons/svg/919/919825.svg"
							      				/>
							      		 </AppearLaterOnScroll6>

							      		<AppearLaterOnScroll5 >
								      		<Avatar 
								      				variant="rounded"
								      				className={classes.skills}
								      				src="https://www.pngitem.com/pimgs/m/664-6644509_icon-react-js-logo-hd-png-download.png"
								      		/>
								      	</AppearLaterOnScroll5>


								      	<AppearLaterOnScroll5 >
								      		<Avatar 
								      				variant="rounded"
								      				className={classes.skills}
								      				src="https://cdn-media-1.freecodecamp.org/images/1*FDNeKIUeUnf0XdqHmi7nsw.png"
								      		/>
								       </AppearLaterOnScroll5>

								       <AppearLaterOnScroll6 >
								      		<Avatar 
								      				variant="rounded"
								      				className={classes.skills}
								      				src="https://cdn.iconscout.com/icon/free/png-512/sass-226054.png"
								      		/>
								       </AppearLaterOnScroll6>

								        

						      		</Toolbar>

						      
			  						</Box>
		  						
			      		</Toolbar> 
			      		
			    </Paper>
		    </AppearOnScroll>
    </React.Fragment>
  )
}

export default ProfileDescription;