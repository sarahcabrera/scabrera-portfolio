import React from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { makeStyles, withStyles, useTheme  } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Grow from '@material-ui/core/Grow';
import Toolbar from '@material-ui/core/Toolbar';
import Avatar from '@material-ui/core/Avatar';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Zoom from '@material-ui/core/Zoom';
import Box from '@material-ui/core/Box';



const useStyles = makeStyles((theme) => ({
  paper1: {
	  width: 300,
	  height: "100%",
	  marginRight: 30,
   borderRadius: 30,
   '&:hover': {
       animation: '$ripple 1.2s 1 ease-in-out',
    },
  },
  paper2: {
	  width: 300,
	  height: "100%",
	  margin: 0,
   borderRadius: 30,
    '&:hover': {
       animation: '$ripple 1.2s 1 ease-in-out',
    },
  },
  paper3: {
	  width: 300,
	  height: "100%",
	  marginLeft: 30,
   borderRadius: 30,
    '&:hover': {
       animation: '$ripple 1.2s 1 ease-in-out',
    },
  },
  image: {
		  width: "100%",
		  height: "100%",
	   borderRadius: 30,
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.85)',
      opacity: .6,
      marginBottom: 0
    },
    '100%': {
      transform: 'scale(1.2)',
      opacity: 1,
      marginBottom: 20
    },
  },
  
	
	
}));

function AppearOnScroll(props) {
	  const { children, window } = props;
	  const trigger = useScrollTrigger({
		    target: window ? window() : undefined,
		    disableHysteresis: true,
		    threshold: 50
	   });  

	  return (
	    <Zoom in={trigger}
	    		style={{ transitionDelay: trigger ? '400ms' : '0ms' }}
	    >
	      <div >
	        {children}
	      </div>
	    </Zoom>
	  );
}

function AppearLaterOnScroll1(props) {
	  const { children, window } = props;
	  const trigger = useScrollTrigger({
		    target: window ? window() : undefined,
		    disableHysteresis: true,
		    threshold: 80
	   });

	  return (
	    <Zoom in={trigger}
	    		style={{ transitionDelay: trigger ? '750ms' : '0ms' }}
	    >
	      <div >
	        {children}
	      </div>
	    </Zoom>
	  );
}

function AppearLaterOnScroll2(props) {
	  const { children, window } = props;
	  const trigger = useScrollTrigger({
		    target: window ? window() : undefined,
		    disableHysteresis: true,
		    threshold: 80
	   });

	  return (
	    <Zoom in={trigger}
	    		style={{ transitionDelay: trigger ? '850ms' : '0ms' }}
	    >
	      <div >
	        {children}
	      </div>
	    </Zoom>
	  );
}

function AppearLaterOnScroll3(props) {
	  const { children, window } = props;
	  const trigger = useScrollTrigger({
		    target: window ? window() : undefined,
		    disableHysteresis: true,
		    threshold: 80
	   });

	  return (
	    <Zoom in={trigger}
	    		style={{ transitionDelay: trigger ? '950ms' : '0ms' }}
	    >
	      <div >
	        {children}
	      </div>
	    </Zoom>
	  );
}



const Project = (props) => {

  const classes = useStyles();

  const theme = useTheme();



  return (

    <React.Fragment>
    	<AppearOnScroll {...props}>
    	
			      		<Toolbar >
				      			
				      	<Box width="100%">
						      			<Typography variant="h6" 
						      									align="center"

						      			>
						      					Projects
						      			</Typography>
						      </Box>
				      	
			      		</Toolbar>		

			      		<Toolbar >
			   
						      		<Toolbar>
							      		<AppearLaterOnScroll1 {...props}>

							      				<a href="https://sarahcabrera.gitlab.io/shedcreate-capstone1/index.html" 
							      										target="_blank">
								      				<Paper elevation={10}
								      								className={classes.paper1}
								      				>
											      	
											      			<img src="https://scabrera-portfolio.s3.us-east-1.amazonaws.com/shedcreate.jpg"
											      								width="99%" className={classes.image}  />
								      				</Paper>
							      				</a>
							      		</AppearLaterOnScroll1>

							      		<AppearLaterOnScroll2 {...props}>
									      			<a href="https://ipdatamanager.herokuapp.com/"  
									      								target="_blank">
								      				<Paper elevation={10}
								      								className={classes.paper2}
								      				>
											      	
											      			<img src="https://scabrera-portfolio.s3.us-east-1.amazonaws.com/ipdatamanager.jpg"
											      								width="99%"  className={classes.image}  />
								      				</Paper>
							      				</a>
						      			</AppearLaterOnScroll2>

						      			<AppearLaterOnScroll3 {...props}>
									      			<a href="https://serve-focus.herokuapp.com"   
									      									target="_blank">
								      				<Paper elevation={10}
								      								className={classes.paper3}
								      				>
											      	
											      			<img src="https://scabrera-portfolio.s3.us-east-1.amazonaws.com/servefocus.jpg"
											      								width="99%"  className={classes.image}  />
								      				</Paper>
							      				</a>
								      	</AppearLaterOnScroll3>

								      

						      		</Toolbar>

		  						
			      		</Toolbar> 
			      		
			 
		    </AppearOnScroll>
    </React.Fragment>
  )
}

export default Project;