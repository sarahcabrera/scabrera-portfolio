import React, {useState} from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, withStyles  } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Fab from '@material-ui/core/Fab';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Zoom from '@material-ui/core/Zoom';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import FaceIcon from '@material-ui/icons/Face';
import BurstModeIcon from '@material-ui/icons/BurstMode';
import Avatar from '@material-ui/core/Avatar';
import { Link, NavLink } from 'react-router-dom';
import Badge from '@material-ui/core/Badge';
import { blue } from '@material-ui/core/colors';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { Switch, FormControlLabel } from '@material-ui/core';
import Brightness4TwoToneIcon from '@material-ui/icons/Brightness4TwoTone';
import ButtonGroup from '@material-ui/core/ButtonGroup';

import Landing from './Landing';
import ProfileDescription from './ProfileDescription';
import Project from './Project';

import DarkModeToggle from "react-dark-mode-toggle";




const useStyles = makeStyles((theme) => ({
  root: {
    position: 'fixed',
    bottom: theme.spacing(4),
    right: theme.spacing(5),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    '& > *': {
      margin: theme.spacing(1),
      },
  },
  button: {
    margin: theme.spacing(3),
  },
  projects: {
    width: theme.spacing(9),
    height: theme.spacing(9),
    margin: 5,
    border: '1px solid gold',
    backgroundColor: 'mediumorchid',
    color: 'pink',
  },
  profile: {
    width: theme.spacing(9),
    height: theme.spacing(9),
    border: '1px solid gold',
    marginLeft: 10,
 
  },
  toggle: {
  	position: 'absolute',
  	top: 45,
  	left: 5,
  	borderRadius: 50,
  	border: '1px solid gold',
  },
  navbar: {
  	height: 100,
  },
  navtext: {
  	margin: 7
  },
  scrollUp: {
  	boxShadow: ' 1px 1px 3px 1px pink',
  	border: '1px solid gold',
  	backgroundColor: 'mediumorchid',
    color: 'pink',
  },
  container: {
  	boxShadow: ' 2px 2px 2px 2px gold',

  },


}));

const StyledBadge = withStyles((theme) => ({
  badge: {
  	backgroundColor: 'currentColor',
    position: 'absolute',
      top: 58,
      left: 65,
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: '$ripple 1.2s infinite ease-in-out',
      border: '3px solid currentColor',
      content: '""',
    },
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.9)',
      opacity: 1,
    },
    '100%': {
      transform: 'scale(2.9)',
      opacity: 0,
    },
  },
}))(Badge);


function ScrollTop(props) {
  const { children, window } = props;
  const classes = useStyles();
  const trigger = useScrollTrigger({
	    target: window ? window() : undefined,
	    disableHysteresis: true,
	    threshold: 25
   });

    const handleClick = (event) => {
  	const anchor = (event.target.ownerDocument || document).querySelector('#back-to-top-anchor');

  	if (anchor) {
  	   anchor.scrollIntoView({ behavior: 'smooth', block: 'center' });
  	}
  };

  return (
    <Zoom in={trigger}>
      <div onClick={handleClick} role="presentation" className={classes.root}>
        {children}
      </div>
    </Zoom>
  );
}


 const handleClickProfile = (event) => {
    const anchor = (event.target.ownerDocument || document).querySelector('#profile');

    if (anchor) {
       anchor.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  };

  const handleClickProjects = (event) => {
    const anchor = (event.target.ownerDocument || document).querySelector('#projects');

    if (anchor) {
       anchor.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  };

   const handleClickContact = (event) => {
    const anchor = (event.target.ownerDocument || document).querySelector('#contact');

    if (anchor) {
       anchor.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  };



function HideOnScroll(props) {
  const { children, window } = props;
  const trigger = useScrollTrigger({
	    target: window ? window() : undefined,
	    disableHysteresis: true,
	    threshold: 5
	});

  return (
    <Slide
        appear={false} 
        direction="down" 
        in={!trigger}>
      {children}
    </Slide>
  );
}



const themeObject = ({
    palette: {
      primary: {
      	light: '#73e8ff',
        main: '#29b6f6',
        dark: '#0086c3',
        
      },
      secondary: {
      	light: '#ff7961',
        main: '#f44336',
        dark: '#ba000d',
       
      },
      type: 'light',
     
    },

  });

const useDarkMode = () => {
    const [theme, setTheme] = useState(themeObject);

    const { palette : { type }} = theme;

    const toggleDarkMode = () => {
      const updatedTheme = {
        ...theme,
        palette: {
          ...theme.palette,
          type: type === 'light' ? 'dark' : 'light'
        }
      }
      setTheme(updatedTheme);
    }

    return [theme, toggleDarkMode]
}

const MainNav = (props) => {

  const classes = useStyles();
  const [theme, toggleDarkMode] = useDarkMode();
  const themeConfig = createMuiTheme(theme);
  const [isDarkMode, setIsDarkMode] = useState(() => false);


  return (
  	<MuiThemeProvider theme={themeConfig} >
  		
	      <CssBaseline  />
	       <HideOnScroll {...props}>
		      <AppBar   
              className={classes.navbar} 
              color="transparent"
              
              style={{ 
                backgroundImage: theme.palette.type === 'light' ? 
                  "url('https://scabrera-portfolio.s3.us-east-1.amazonaws.com/pexels-jess-bailey-designs-876467.jpg')" : 
                  "url('https://scabrera-portfolio.s3.us-east-1.amazonaws.com/pexels-johannes-plenio-3262249.jpg')",
                borderRadius: 0, 
                color: theme.palette.type === 'light' ? 'black' : 'white', 
              }}
		      >


           
		        <Box >
				        <Link className="mx-3" to="/profile" 
                      style={{ textDecoration:'initial' }}>
				        	<Button
                    onClick={handleClickProfile}
                    >
			        			<StyledBadge
							        overlap="round"
							        anchorOrigin={{
							          vertical: 'bottom',
							          horizontal: 'right',
							        }}
							       variant="dot"
							       style={{color: theme.palette.type === 'light' ? "#44b700" : "lightgrey"}}
							    >
							    	
					        		<Avatar alt="S.Cabrera" 
					        			variant="circle" 
					        			className={classes.profile}
					        			src={theme.palette.type === 'light' ?
                           'https://scabrera-portfolio.s3.amazonaws.com/stcprofile2.jpg' :
                          'https://scabrera-portfolio.s3.amazonaws.com/stcprofile3.jpg'
                          } 
					        			/>  
							    	
					        	</StyledBadge>
					        	<Typography className={classes.navtext}>Profile </Typography>
						    </Button>
				    	</Link>
				        <Link className="mx-3" to="/projects" style={{ textDecoration:'initial' }}>
					        <Button 
                    onClick={handleClickProjects}
                  >
					        	<Avatar 
                        className={classes.projects}
                        src={theme.palette.type === 'light' ?
                          'https://scabrera-portfolio.s3.amazonaws.com/stcprofile17.jpg' :
                          'https://scabrera-portfolio.s3.amazonaws.com/macbookdark.jpg'
                          } 
                    >
					        		
					        	</Avatar>
						    		<Typography className={classes.navtext}>Projects</Typography>
					      	</Button>
				      	</Link>
				  
					     
		        </Box>
		      </AppBar>
		    </HideOnScroll>
	      <Toolbar id="back-to-top-anchor" />
	      <Container maxWidth="100%" maxHeight="100%"
	      			className={classes.landing}
	      			my={0} mx={20} 
			      	style={{ 
			      	
			      		backgroundImage: theme.palette.type === 'light' ? 
                  "url('https://scabrera-portfolio.s3.us-east-1.amazonaws.com/pexels-jess-bailey-designs-876467.jpg')" : 
                  "url('https://scabrera-portfolio.s3.us-east-1.amazonaws.com/pexels-johannes-plenio-3262249.jpg')",
			      		borderRadius: 0, 
			      		color: theme.palette.type === 'light' ? 'black' : 'white', 
			      	}}
	      >

	      	 <Button onClick={toggleDarkMode}>
		        <FormControlLabel 
		      		control={
						    <DarkModeToggle
						      onChange={setIsDarkMode} 
						      checked={isDarkMode}
						      size={40}
						      className={classes.toggle}
						    />
						     } 
			     className={classes.toggle}
			      	 />
		     </Button>


	        <Box 
              my={5} mx={15} 
	        	 px={35} py={30}
	        	style={{color: theme.palette.type === 'light' ? '#870000' : '#fff1ff',
                    borderRadius: 15}}
	        	>
			       <Landing theme={theme}/>
	        </Box>

          
            <Box my={5} mx={20} 
               px={5} py={7}
              
               id="profile"
              >
              
               <ProfileDescription theme={theme} />
             
            </Box>

            <Box my={5} mx={20} 
               px={5} py={7}
          
               id="projects"
              >
              
               <Project theme={theme}/>

               

            </Box>


            <Box 
                my={5} mx={20} 
               px={5} py={7}
          
               id="projects"
              >
              
              <br/>
           

            </Box>

     
  	      <ScrollTop {...props}>
  	        <Fab color="secondary" 
  	        	size="large" 
  	        	aria-label="scroll back to top" 
  	        	className={classes.scrollUp} 
  	        	
  	        >
  	          <KeyboardArrowUpIcon style={{ fontSize: 40 }} />
  	        </Fab>
  	      </ScrollTop>

	      </Container>

    </MuiThemeProvider>
  );
}

export default MainNav;

