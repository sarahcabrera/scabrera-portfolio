import React, {useState} from 'react';
import { Button } from '@material-ui/core';
import MainNav from './components/MainNav';
import Landing from './components/Landing';
import ProfileDescription from './components/ProfileDescription';
import Projects from './components/Project';
import {v1 as uuid} from 'uuid';
import { 
  BrowserRouter as Router,
  Switch,
  Route 
} from "react-router-dom";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';



function App() {

 

  const [projects, setProjects] = useState([
      {
        name: "She'd Create - She Sheds for Creative Souls", 
        id: uuid(),
        url: "https://sarahcabrera.gitlab.io/shedcreate-capstone1",
        image: "./../public/assets/shedcreate.jpg",
        description: "A responsive static website created with HTML, CSS, and Bootstrap."
      },
      {
        name: "IPDataManager", 
        id: uuid(),
        url: "https://http://ipdatamanager.herokuapp.com",
        image: "./../public/assets/ipdatamanager.jpg",
        description: "An intellectual property asset management webapp built with PHP, Laravel, MySQL, and deployed in Heroku."
      }
      
     
  ]);

 const [profile, setProfile] = useState([
      {
        name: "Sarah Cabrera", 
        id: uuid(),
        image: "./../public/assets/stcprofile3.jpg",
        email: "scabreradeveloper@gmail.com",
        linkedIn: "https://www.linkedin.com/in/sarah-c-975734b",
        instagram: "https://www.instagram.com/sarahcabdev",
        gitlab: "https://gitlab.com/sarahcabrera",
        shortBio: "Fullstack Web Developer | Privacy Professional | Lawyer",
        description: "A fullstack web developer, privacy professional (CIPM and CIPP/E), lawyer, and psychology major who loves gardening, cooking, and painting and design.",
        education: "University of the Philippines, Zuitt Coding Bootcamp"
      }
  ]);

  
  return (


      <Router>

         <MainNav />

     {/*   <Switch>

            <Route exact path="/landing">
             
              <Landing   />
            </Route>

            <Route exact path="/profile">
           
              <ProfileDescription />
            </Route>

            <Route path="/projects">

              <Projects />
            </Route>
        </Switch>*/}

      </Router>

  );
}

export default App;
